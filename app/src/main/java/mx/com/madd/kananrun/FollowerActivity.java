package mx.com.madd.kananrun;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;


import android.webkit.CookieManager;
import android.widget.Toast;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class FollowerActivity extends AppCompatActivity {

    String url;
    private WebView webView;
    String cookie_user;
    List<Kanan> kanan;

    private Toast mToastToShow;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // This code Add Menu options
        getMenuInflater().inflate(R.menu.menu_follower, menu);
        return true;
    }

    private boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        kanan = Kanan.listAll(Kanan.class);
        cookie_user="login_userName=run"+ kanan.get(0).getRunner_number();

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follower);
        Context context = this.getApplicationContext();


        String url = "http://app.trackermexico.com.mx/smartphonemain.aspx";
        if (haveNetworkConnection(context))
        {
            webView = (WebView) findViewById(R.id.webView1);
            final WebSettings settings = webView.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setAllowFileAccess(true);
            settings.setAppCacheEnabled(true);
            settings.setBuiltInZoomControls(true);
            settings.setGeolocationEnabled(true);
            settings.setSupportMultipleWindows(true);

            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.ECLAIR) {
                try {
                    Method m1 = WebSettings.class.getMethod("setDomStorageEnabled", new Class[]{Boolean.TYPE});
                    m1.invoke(settings, Boolean.TRUE);

                    Method m2 = WebSettings.class.getMethod("setDatabaseEnabled", new Class[]{Boolean.TYPE});
                    m2.invoke(settings, Boolean.TRUE);

                    Method m3 = WebSettings.class.getMethod("setDatabasePath", new Class[]{String.class});
                    m3.invoke(settings, "/data/data/" + getPackageName() + "/databases/");

                    Method m4 = WebSettings.class.getMethod("setAppCacheMaxSize", new Class[]{Long.TYPE});
                    m4.invoke(settings, 1024*1024*8);

                    Method m5 = WebSettings.class.getMethod("setAppCachePath", new Class[]{String.class});
                    m5.invoke(settings, "/data/data/" + getPackageName() + "/cache/");

                    Method m6 = WebSettings.class.getMethod("setAppCacheEnabled", new Class[]{Boolean.TYPE});
                    m6.invoke(settings, Boolean.TRUE);

                }
                catch (NoSuchMethodException e) {
                }
                catch (InvocationTargetException e) {
                }
                catch (IllegalAccessException e) {
                }
            }

            webView.setWebViewClient(new WebViewClient());
            try{
                webView.setAccessibilityHeading(true);
            }catch (Error e){
            }

            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            CookieManager.getInstance().setCookie(url, "login_autoLogin=true");
            CookieManager.getInstance().setCookie(url, "login_password=aniversario");
            CookieManager.getInstance().setCookie(url, cookie_user);

            webView.loadUrl("http://app.trackermexico.com.mx/smartphonemain.aspx");
            //webView.loadData(postData,"text/html", "utf-8");
        }
        else
        {
            Toast.makeText(context, "No internet", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_go_back) {

            int toastDurationInMilliSeconds = 10000;
            mToastToShow = Toast.makeText(this, "Instrucciones: De click en el Menu Interior Dispositivo, Seleccione el número de celular, de click" +
                    " en el Menu Inferior Rastrear !LISTO¡ ya esta rastreando a su corredor.", Toast.LENGTH_LONG);

            // Set the countdown to display the toast
            CountDownTimer toastCountDown;
            toastCountDown = new CountDownTimer(toastDurationInMilliSeconds, 10000 /*Tick duration*/) {
                public void onTick(long millisUntilFinished) {
                    mToastToShow.show();
                }
                public void onFinish() {
                    mToastToShow.cancel();
                }
            };

            // Show the toast and starts the countdown
            mToastToShow.show();
            toastCountDown.start();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
