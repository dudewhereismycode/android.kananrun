package mx.com.madd.kananrun;

import com.orm.SugarRecord;

public class Kanan extends SugarRecord{
    String runner_number;

    public Kanan(){}

    public Kanan(String runner_number){
        this.runner_number = runner_number;
    }

    public String getRunner_number() {
        return runner_number;
    }


}
