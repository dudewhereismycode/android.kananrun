package mx.com.madd.kananrun;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class MainActivity extends AppCompatActivity implements OnClickListener {

    Date currentTime = Calendar.getInstance().getTime();
    String dtStart = "2019-08-18T07:00:00Z";
    private TextView mTextViewCountDown;
    private TextView mTextViewDaysLeft;
    private long mTimeLeft = 6000;
    private CountDownTimer mCountDownTimer;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
        //Convierte el dia de la Carrera en Microsegundos
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date runDate = Calendar.getInstance().getTime();
        try{
            runDate = format.parse(dtStart);
        }catch(ParseException ex){
        }

        //Calcula los microsegundos que faltan para el dia de la carrera e inicializa las variables
        long diff = runDate.getTime() - currentTime.getTime();
        mTimeLeft = runDate.getTime() - currentTime.getTime();
        mTextViewCountDown = findViewById(R.id.countdown);
        mTextViewDaysLeft = findViewById(R.id.daysleft);

        //Llama a la funcion que disminuye el conteo
        startTime();
*/
        //Inicializa el click de los botones
        View button_runner = findViewById(R.id.im_runner);
        button_runner.setOnClickListener(this);
        View button_not_runner = findViewById(R.id.not_runner);
        button_not_runner.setOnClickListener(this);

        //Pedir permisos
        checkLocationPermission();
        if(googleServicesAvailable()){
            Toast.makeText(this,"Perfect!!!",Toast.LENGTH_LONG).show();
        };

    }

    //Escucha del evento
    @Override
    public void onClick(View v){
        if(v.getId()==findViewById(R.id.im_runner).getId()){
            lanch_im_eagle();
        }else if(v.getId()==findViewById(R.id.not_runner).getId()){
            lanch_im_kanan();
        }
    }

    /*
    private void startTime(){
        mCountDownTimer = new CountDownTimer(mTimeLeft,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTimeLeft = millisUntilFinished;
                updateCounterDownText();
            }

            @Override
            public void onFinish() {

            }
        }.start();
    }

    private void updateCounterDownText(){
        long diff = mTimeLeft;
        long days = diff / (24 * 60 * 60 * 1000);
        diff -= days * (24 * 60 * 60 * 1000);
        long hours = diff / (60 * 60 * 1000);
        diff -= hours * (60 * 60 * 1000);
        long minutes = diff / (60 * 1000);
        diff -= minutes * (60 * 1000);
        long seconds = diff / 1000;
        String timeLeftString = String.format(Locale.getDefault(), "%02d:%02d:%02d",hours,minutes,seconds);
        mTextViewCountDown.setText(timeLeftString);
        mTextViewDaysLeft.setText(String.format(Locale.getDefault(), "%02d días",days));

    }
*/
    //MENU OPCIONES
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    //MENU ACCIONES
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.who_we_are) {
            startActivity(new Intent(getApplicationContext(),WhoWeAre.class));
            return true;
        }else if (id == R.id.about) {
            startActivity(new Intent(getApplicationContext(),About.class));
            return true;
        }else if (id == R.id.instructions) {
            startActivity(new Intent(getApplicationContext(),Instructions.class));
            return true;
        }
            return super.onOptionsItemSelected(item);
    }

    //ACCION DE BOTONES
    public void lanch_im_eagle(){
        Intent im_eagle = new Intent(this,EagleActivity.class);
        startActivity(im_eagle);
    }

    public void lanch_im_kanan(){
        Intent im_kanan = new Intent(this,KananActivity.class);
        startActivity(im_kanan);
    }

    public void lanch_historial(View view){
        Intent historial = new Intent(this,Historial.class);
        startActivity(historial);
    }


    //PEDIR PERMISO DE UBICACION
    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("¿Permitir Ubicación para KANANrunning?")
                        .setMessage("Es necesario para poder compartir la ruta")
                        .setPositiveButton("PERMITIR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, "Esta listo para correr", Toast.LENGTH_LONG).show();
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Se necesitara este permiso más tarde", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    //VALIDAR GOOGLE SERVICES
    public boolean googleServicesAvailable(){
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if(isAvailable == ConnectionResult.SUCCESS){
            return true;
        }else if(api.isUserResolvableError(isAvailable)){
            Dialog dialog = api.getErrorDialog(this,isAvailable,0);
            dialog.show();
        }else{
            Toast.makeText(this,"No se puede conectar a los servicios de GooglePlay",Toast.LENGTH_LONG).show();
        }
        return false;
    }
}
