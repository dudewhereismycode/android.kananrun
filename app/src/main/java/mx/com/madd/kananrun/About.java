package mx.com.madd.kananrun;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }

    public void kananapp(View view){
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=mx.com.madd.kanan" )));
    }
    public void angelapp(View view){
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=mx.com.madd.kananangel" )));
    }
    public void kananurl(View view){
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://kanangps.mx/" )));
    }
    public void kananface(View view){
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/kanangps/" )));
    }
}
