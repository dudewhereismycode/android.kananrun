package mx.com.madd.kananrun;

import android.Manifest;
import android.app.AlertDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class JobActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {
    private static final String TAG = "JobActivity";

    GoogleMap mGoogleMap;
    protected LocationManager locationManager;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    List<Eagle> eagles;
    Marker marker;
    TextView txtLat;

    String url_dwim;

    float diff;
    float first;
    int az;
    int on_play;

    String latitud;
    String longitud;

    Location loc1;
    Location loc2;

    Date hor1;
    Date hor2;

    Button button_start;
    Button button_stop;
    String msg = "MSG : ";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job);
        eagles = Eagle.listAll(Eagle.class);
        txtLat = (TextView) findViewById(R.id.textview1);
        button_start = (Button) findViewById(R.id.button_start);
        button_stop = (Button) findViewById(R.id.button_stop);
        url_dwim="http://dwim-angel.dwim.mx/api/noa/updatekananloc";

        on_play = 0;
        diff = 4;
        loc1 = new Location("");
        loc2 = new Location("");

        hor1 = Calendar.getInstance().getTime();
        hor2 = Calendar.getInstance().getTime();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        checkLocationPermission();
        initMap();
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        goToLocationZoom(19.4526231,-99.1721658, 17);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);

    }

    private void goToLocationZoom(double lat, double lon,float zoom){
        LatLng ll = new LatLng(lat,lon);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll,zoom);
        mGoogleMap.moveCamera(update);
    }

    public void playRoute(){
        if(on_play == 1){
            on_play = 0;
            Toast.makeText(this, "Actividad Detenida", Toast.LENGTH_LONG).show();
        }else{
            on_play = 1;
            Toast.makeText(this, "Actividad Iniciada", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLocationChanged(android.location.Location location) {
        Log.d(msg, location.getLatitude() + "," + location.getLongitude());
        txtLat.setText(location.getLatitude() + "," + location.getLongitude()+" Diferencia: "+diff);
        if(location==null){
            Toast.makeText(this,"No se encuentra ubicación",Toast.LENGTH_LONG).show();
        }else{
            if(diff>=3) {
                LatLng ll = new LatLng(location.getLatitude(), location.getLongitude());
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 17);
                mGoogleMap.animateCamera(update);
                if (marker != null) {
                    marker.remove();
                }
                if (on_play != 0) {
                    MarkerOptions options = new MarkerOptions()
                            .title(eagles.get(0).getName())
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                            .position(ll);
                    marker = mGoogleMap.addMarker(options);
                }
            }
            if (on_play!= 0){
                latitud = Double.toString(location.getLatitude());
                longitud = Double.toString(location.getLongitude());
                if(first==0){
                    loc1.setLatitude(location.getLatitude());
                    loc1.setLongitude(location.getLongitude());
                    hor1 = Calendar.getInstance().getTime();
                    loc2.setLatitude(location.getLatitude());
                    loc2.setLongitude(location.getLongitude());
                    hor2 = Calendar.getInstance().getTime();
                    txtLat.setText("Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude()+", Speed:"+location.hasSpeed());
                    first=1;
                }
                diff = loc1.distanceTo(loc2);
                if(diff>=5){
                    az=0;
                    loc1.setLatitude(location.getLatitude());
                    loc1.setLongitude(location.getLongitude());
                    hor1 = Calendar.getInstance().getTime();
                    txtLat.setText("Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude());
                }
                loc2.setLatitude(location.getLatitude());
                loc2.setLongitude(location.getLongitude());
                hor2 = Calendar.getInstance().getTime();
            }
        }
    }

    public void scheduleJob(View v) {
        button_start.setEnabled(false);
        button_stop.setEnabled(true);

        ComponentName componentName = new ComponentName(this, LocationJobService.class);
        JobInfo info = new JobInfo.Builder(123, componentName)
                .setRequiresCharging(false)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .setPeriodic(15 * 60 * 1000)
                .build();

        JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        int resultCode = scheduler.schedule(info);
        if (resultCode == JobScheduler.RESULT_SUCCESS) {
            Log.d(TAG, "Job scheduled");
            playRoute();
            Toast.makeText(this, "Actividad Iniciada", Toast.LENGTH_LONG).show();
        } else {
            Log.d(TAG, "Job scheduling failed");
        }
    }

    public void cancelJob(View v) {
        button_start.setEnabled(true);
        button_stop.setEnabled(false);
        JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        scheduler.cancel(123);
        Log.d(TAG, "Job cancelled");
        playRoute();
        Toast.makeText(this, "Actividad Detenida", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    //PEDIR PERMISO DE UBICACION
    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("¿Permitir Ubicación para KANANrunning?")
                        .setMessage("Es necesario para poder compartir la ruta")
                        .setPositiveButton("PERMITIR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(JobActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Activa el permiso de ubicación para esta app", Toast.LENGTH_LONG).show();
                    Intent im_kanan = new Intent(this,MainActivity.class);
                    startActivity(im_kanan);
                }
                return;
            }
        }
    }
}