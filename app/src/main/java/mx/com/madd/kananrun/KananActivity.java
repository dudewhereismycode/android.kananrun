package mx.com.madd.kananrun;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.List;

public class KananActivity extends AppCompatActivity {

    EditText runner_number2;

    Button follow;
    Button unfollow;
    Button start;

    String sun_url = "https://sun.dudewhereismy.com.mx";
    String error;

    LinearLayout loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kanan);
        runner_number2 = (EditText) findViewById(R.id.runner_number2);
        runner_number2.setEnabled(true);

        follow = (Button) findViewById(R.id.follow);
        unfollow = (Button) findViewById(R.id.unfollow);
        start = (Button) findViewById(R.id.start_follow);

        loader = findViewById(R.id.loader);
        loader.setVisibility(View.GONE);

        List<Kanan> kanans = Kanan.listAll(Kanan.class);

        if (kanans.isEmpty()){
            unfollow.setVisibility(View.GONE);
            start.setVisibility(View.GONE);
            follow.setVisibility(View.VISIBLE);
        }else{
            runner_number2.setText(kanans.get(0).getRunner_number());
            runner_number2.setEnabled(false);
            unfollow.setVisibility(View.VISIBLE);
            start.setVisibility(View.VISIBLE);
            follow.setVisibility(View.GONE);
        }

    }

    public void followMyEagle(){
        if(error.length()==2){
            String myrunner_number = runner_number2.getText().toString();

            Kanan runner = new Kanan(myrunner_number);
            runner.save();
            Toast.makeText(this, "Siguiendo Corredor", Toast.LENGTH_LONG).show();

            runner_number2.setEnabled(false);

            unfollow.setVisibility(View.VISIBLE);
            start.setVisibility(View.VISIBLE);
            follow.setVisibility(View.GONE);
            loader.setVisibility(View.GONE);
        }else{
            follow.setVisibility(View.VISIBLE);
            loader.setVisibility(View.GONE);
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }

    }

    public void followEagle(View v){
        loader.setVisibility(View.VISIBLE);
        start.setVisibility(View.GONE);
        unfollow.setVisibility(View.GONE);
        follow.setVisibility(View.GONE);
        String myrunner_number = runner_number2.getText().toString();
        if(myrunner_number.length()>0){
            String url = sun_url +"/services/kananRunExist";
            String postData = "runner_number="+myrunner_number;
            Http web_service = new Http(url,"PUT",postData,getApplicationContext(), new OnEventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response) {
                    if (response.has("error")) {
                        error =  response.optString("error");
                        followMyEagle();
                    }
                }
                @Override
                public void onFailure(Exception e) {
                }
            });
            web_service.execute();

        }
        else{
            follow.setVisibility(View.VISIBLE);
            loader.setVisibility(View.GONE);
            Toast.makeText(this,"Número de Corredor obligatorio",Toast.LENGTH_LONG).show();
        }
        //loader.setVisibility(View.GONE);
    }

    public void unfollowEagle(View v){

        Kanan.deleteAll(Kanan.class);

        runner_number2.setText("");
        runner_number2.setEnabled(true);

        unfollow.setVisibility(View.GONE);
        start.setVisibility(View.GONE);
        follow.setVisibility(View.VISIBLE);
        loader.setVisibility(View.GONE);
    }

    public void lanch_follower(View view){
        /*Intent im_kanan = new Intent(this,LocationActivity.class);
        startActivity(im_kanan);*/

        Intent intent = new Intent(getApplicationContext(), FollowerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
