package mx.com.madd.kananrun;
import com.orm.SugarRecord;

public class Eagle extends SugarRecord{
    String name;
    String phone;
    String email;
    String runner_number;

    public Eagle(){}

    public Eagle(String name, String phone, String email, String runner_number){
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.runner_number = runner_number;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getRunner_number() {
        return runner_number;
    }
}
