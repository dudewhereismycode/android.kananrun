package mx.com.madd.kananrun;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.List;

public class EagleActivity extends AppCompatActivity {

    EditText name;
    EditText phone;
    EditText email;
    EditText runner_number;
    LinearLayout loader;

    Button create;
    Button delete;
    Button start;

    String sun_url = "https://sun.dudewhereismy.com.mx";
    String error;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eagle);
        name = (EditText) findViewById(R.id.runner_name);
        phone = (EditText) findViewById(R.id.runner_phone);
        email = (EditText) findViewById(R.id.runner_email);
        runner_number = (EditText) findViewById(R.id.runner_number);

        loader = findViewById(R.id.loader);
        loader.setVisibility(View.GONE);

        create = (Button) findViewById(R.id.button_add);
        delete = (Button) findViewById(R.id.button_delete);
        start = (Button) findViewById(R.id.start_activity);

        List<Eagle> eagles = Eagle.listAll(Eagle.class);

        if (eagles.isEmpty()){
            delete.setVisibility(View.GONE);
            start.setVisibility(View.GONE);
            create.setVisibility(View.VISIBLE);
        }else{
            name.setText(eagles.get(0).getName());
            name.setEnabled(false);
            phone.setText(eagles.get(0).getPhone());
            phone.setEnabled(false);
            email.setText(eagles.get(0).getEmail());
            email.setEnabled(false);
            runner_number.setText(eagles.get(0).getRunner_number());
            runner_number.setEnabled(false);

            delete.setVisibility(View.VISIBLE);
            start.setVisibility(View.VISIBLE);
            create.setVisibility(View.GONE);
        }

    }

    public void createMyEagle(){
        if(error.length()==2){
            String myname = name.getText().toString();
            String myphone = phone.getText().toString();
            String myemail = email.getText().toString();
            String myrunner_number = runner_number.getText().toString();

            Eagle runner = new Eagle(myname,myphone,myemail,myrunner_number);
            runner.save();

            name.setEnabled(false);
            phone.setEnabled(false);
            email.setEnabled(false);
            runner_number.setEnabled(false);

            delete.setVisibility(View.VISIBLE);
            start.setVisibility(View.VISIBLE);
            create.setVisibility(View.GONE);
            loader.setVisibility(View.GONE);
            Toast.makeText(this, "Corredor Registrado", Toast.LENGTH_LONG).show();
        }else{
            delete.setVisibility(View.GONE);
            start.setVisibility(View.GONE);
            create.setVisibility(View.VISIBLE);
            loader.setVisibility(View.GONE);
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }



    }
    public void createEagle(View v){
        loader.setVisibility(View.VISIBLE);
        delete.setVisibility(View.GONE);
        start.setVisibility(View.GONE);
        create.setVisibility(View.GONE);

        String myname = name.getText().toString();
        String myphone = phone.getText().toString();
        String myemail = email.getText().toString();
        String myrunner_number = runner_number.getText().toString();
        if(myname.length()>0){
            if(myphone.length()>0){
                if(myemail.length()>0){
                    if(myrunner_number.length()>0){
                        String url = sun_url +"/services/kananRun";
                        String postData = "runner_number="+myrunner_number+"&runner_email="+myemail+"&runner_name="+myname+"&runner_phone="+myphone;
                        Http web_service = new Http(url,"PUT",postData,getApplicationContext(), new OnEventListener<JSONObject>() {
                            @Override
                            public void onSuccess(JSONObject response) {
                                if (response.has("error")) {
                                    error =  response.optString("error");
                                    createMyEagle();
                                }
                            }
                            @Override
                            public void onFailure(Exception e) {

                            }
                        });
                        web_service.execute();

                    }else{
                        create.setVisibility(View.VISIBLE);
                        loader.setVisibility(View.GONE);
                        Toast.makeText(this,"Numero de Corredor obligatorio",Toast.LENGTH_LONG).show();
                    }
                }else{
                    create.setVisibility(View.VISIBLE);
                    loader.setVisibility(View.GONE);
                    Toast.makeText(this,"Correo electrónico obligatorio",Toast.LENGTH_LONG).show();
                }
            }else{
                create.setVisibility(View.VISIBLE);
                loader.setVisibility(View.GONE);
                Toast.makeText(this,"Número de Celular obligatorio",Toast.LENGTH_LONG).show();
            }
        }else{
            create.setVisibility(View.VISIBLE);
            loader.setVisibility(View.GONE);
            Toast.makeText(this,"Nombre del Corredor obligatorio",Toast.LENGTH_LONG).show();
        }

    }

    public void deletePhoneEagle(){
        if(error.length()==2){
            Eagle.deleteAll(Eagle.class);

            name.setText("");
            name.setEnabled(true);
            phone.setText("");
            phone.setEnabled(true);
            email.setText("");
            email.setEnabled(true);
            runner_number.setText("");
            runner_number.setEnabled(true);

            delete.setVisibility(View.GONE);
            start.setVisibility(View.GONE);
            create.setVisibility(View.VISIBLE);
            loader.setVisibility(View.GONE);
        }
        else{
            loader.setVisibility(View.GONE);
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }

    }

    public void deleteEagle(View v){

        loader.setVisibility(View.VISIBLE);
        delete.setVisibility(View.GONE);
        start.setVisibility(View.GONE);
        create.setVisibility(View.GONE);

        String myname = name.getText().toString();
        String myphone = phone.getText().toString();
        String myemail = email.getText().toString();
        String myrunner_number = runner_number.getText().toString();

        String url = sun_url +"/services/kananRunDelete";
        String postData = "runner_number="+myrunner_number+"&runner_email="+myemail+"&runner_name="+myname+"&runner_phone="+myphone;
        Http web_service = new Http(url,"PUT",postData,getApplicationContext(), new OnEventListener<JSONObject>() {
            @Override
            public void onSuccess(JSONObject response) {
                if (response.has("error")) {
                    error =  response.optString("error");
                    deletePhoneEagle();
                }
            }
            @Override
            public void onFailure(Exception e) {

            }
        });
        web_service.execute();
    }

    public void lanch_location(View view){

        LocationManager lm = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            // notify user
            new AlertDialog.Builder(this)
                    .setMessage("Ubicación desactivada, active para poder comenzar seguimiento")
                    .setPositiveButton("Activar GPS", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            EagleActivity.this.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("Cancelar",null).show();
        }else{

//            Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
//             Intent intent = new Intent(getApplicationContext(), GpsTrackerActivity.class);
            Intent intent = new Intent(getApplicationContext(), JobActivity.class);
            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

    }

}
