package mx.com.madd.kananrun;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    GoogleMap mGoogleMap;
    protected LocationManager locationManager;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    List<Eagle> eagles;
    Marker marker;
    TextView txtLat;

    String url_dwim;

    float diff;
    float first;
    int az;
    int on_play;

    String latitud;
    String longitud;

    Location loc1;
    Location loc2;

    Date hor1;
    Date hor2;

    String msg = "MSG : ";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // This code Add Menu options
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        eagles = Eagle.listAll(Eagle.class);
        txtLat = (TextView) findViewById(R.id.textview1);
        url_dwim="http://dwim-angel.dwim.mx/api/noa/updatekananloc";

        on_play = 1;
        diff = 4;
        loc1 = new Location("");
        loc2 = new Location("");

        hor1 = Calendar.getInstance().getTime();
        hor2 = Calendar.getInstance().getTime();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        checkLocationPermission();
        initMap();
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        goToLocationZoom(19.4526231,-99.1721658, 17);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);

    }

    @Override
    public void onLocationChanged(android.location.Location location) {
        String speed = Float.toString(location.getSpeed());
        Log.d(msg, location.getLatitude() + "," + location.getLongitude()+", SPEED:"+ speed);
        txtLat.setText(location.getLatitude() + "," + location.getLongitude()+" Diferencia: "+diff);
        if(location==null){
            Toast.makeText(this,"No se encuentra ubicación",Toast.LENGTH_LONG).show();
        }else{
            if(diff>=3) {
                LatLng ll = new LatLng(location.getLatitude(), location.getLongitude());
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 17);
                mGoogleMap.animateCamera(update);
                if (marker != null) {
                    marker.remove();
                }
                if (on_play != 0) {
                    MarkerOptions options = new MarkerOptions()
                            .title(eagles.get(0).getName())
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                            .position(ll);
                    marker = mGoogleMap.addMarker(options);
                }
            }
            if (on_play!= 0){
                latitud = Double.toString(location.getLatitude());
                longitud = Double.toString(location.getLongitude());
                if(first==0){
                    loc1.setLatitude(location.getLatitude());
                    loc1.setLongitude(location.getLongitude());
                    hor1 = Calendar.getInstance().getTime();
                    loc2.setLatitude(location.getLatitude());
                    loc2.setLongitude(location.getLongitude());
                    hor2 = Calendar.getInstance().getTime();
                    txtLat.setText("Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude()+", Speed:"+location.hasSpeed());
                    requestMs03(latitud,longitud,speed);
                    first=1;
                }
                diff = loc1.distanceTo(loc2);

                if(diff>=5){
                    az=0;
                    loc1.setLatitude(location.getLatitude());
                    loc1.setLongitude(location.getLongitude());
                    hor1 = Calendar.getInstance().getTime();
                    txtLat.setText("Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude());
                    requestMs03(latitud,longitud,speed);
                }
                loc2.setLatitude(location.getLatitude());
                loc2.setLongitude(location.getLongitude());
                hor2 = Calendar.getInstance().getTime();
            }
        }
    }

    public void playRoute(){
        if(on_play == 1){
            on_play = 0;
            finish();
            Toast.makeText(this, "Actividad Detenida", Toast.LENGTH_LONG).show();
        }else{
            on_play = 1;
            Toast.makeText(this, "Actividad Iniciada", Toast.LENGTH_LONG).show();
        }
    }

    public void requestMs03(String latitud, String longitud,String speed){

        String url = url_dwim+"?imei="+eagles.get(0).getPhone()+"&email="+eagles.get(0).getEmail()+"&group=test&latitude="+latitud+"&longitude="+longitud+"&speed="+speed+"&battery=0&azimuth=0";
        //?imei=551106206&email=maria@dwim.mx&group=test&latitude=19.528006&speed=0&battery=0&azimuth=0&longitude=-99.216983
        String postData = "";
        Http web_service = new Http(url,"GET",postData,getApplicationContext(), new OnEventListener<JSONObject>() {
            @Override
            public void onSuccess(JSONObject response) {
                if (response.has("Error")) {
                }
            }
            @Override
            public void onFailure(Exception e) {
            }
        });
        web_service.execute();

        Toast.makeText(this, "Envie posicion", Toast.LENGTH_LONG).show();
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void goToLocationZoom(double lat, double lon,float zoom){
        LatLng ll = new LatLng(lat,lon);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll,zoom);
        mGoogleMap.moveCamera(update);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_go_back) {
            playRoute();
            if(on_play==1){
                item.setIcon(ContextCompat.getDrawable(this,R.drawable.stop));
            }else{
                finish();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    //PEDIR PERMISO DE UBICACION
    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("¿Permitir Ubicación para KANANrunning?")
                        .setMessage("Es necesario para poder compartir la ruta")
                        .setPositiveButton("PERMITIR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapsActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Activa el permiso de ubicación para esta app", Toast.LENGTH_LONG).show();
                    Intent im_kanan = new Intent(this,MainActivity.class);
                    startActivity(im_kanan);
                }
                return;
            }
        }
    }
}